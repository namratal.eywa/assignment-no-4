const express = require("express");
const router = express.Router();
//const admin = require('firebase-admin');
const User = require("./user");
const auth_middelware = require("../middelware/auth_middelware");

router.get('/', auth_middelware, User.read_csv);
router.post('/login', User.login); 
//router.put('/:id', auth_middelware, User.updateProducts);

module.exports = router;  

