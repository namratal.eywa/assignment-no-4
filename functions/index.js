const functions = require("firebase-functions");
const express = require('express');
const admin = require('firebase-admin');

admin.initializeApp();

const bodyparser = require("body-parser");
const userRoutes = require("./src/routes/route");
const app = express();

app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());
app.use(userRoutes);
app.use(express.json());

exports.employee = functions.https.onRequest(app);

exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});